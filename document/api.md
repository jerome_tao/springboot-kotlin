## /User接口测试/update

#### 接口URL

> http://localhost:8080/user/update

#### 请求方式

> POST

#### Content-Type

> json

#### 请求Body参数

```javascript
{
	"userId": 3,
	"userName": "admin",
	"gender": "0",
	"phone": "15500000000",
	"address": "广州",
	"email": null,
	"password": "123456",
	"status": 0,
	"post": 0,
	"createTime": "2022-06-06T10:16:02",
	"updateTime": "2022-06-06T10:16:05"
}
```

| 参数名          | 示例值      | 参数类型 | 是否必填 | 参数描述                      |
| --------------- | ----------- | -------- | -------- | ----------------------------- |
| userId          | 3           | Number   | 是       | 用户ID                        |
| userName        | admin       | String   | 否       | 用户名                        |
| gender          | 0           | String   | 否       | 性别                          |
| phone           | 15500000000 | String   | 否       | 手机号                        |
| address         | 广州        | String   | 否       | 地址                          |
| email           | -           | String   | 否       | 电子邮箱，比如 example@qq.com |
| password        | 123456      | String   | 否       | 密码                          |
| status          | -           | Number   | 否       | 状态                          |
| post            | -           | Number   | 否       | 职位                          |


## /User接口测试/info

#### 接口URL

> http://localhost:8080/user/info?id=1

#### 请求方式

> GET

#### Content-Type

> form-data

#### 请求Query参数

| 参数名          | 示例值 | 参数类型 | 是否必填 | 参数描述 |
| --------------- | ------ | -------- | -------- | -------- |
| id              | 1      | Text     | 是       | 用户ID   |


#### 成功响应示例

```javascript
{
	"userId": 1,
	"userName": "王五",
	"gender": "0",
	"phone": "15500000000",
	"address": "广州",
	"email": null,
	"password": "123456",
	"status": 0,
	"post": 0,
	"createTime": "2022-06-06T10:16:02",
	"updateTime": "2022-06-06T10:16:05"
}
```

| 参数名                | 示例值              | 参数类型 | 参数描述                      |
| --------------------- | ------------------- | -------- | ----------------------------- |
| userId                | 1                   | Number   | 用户ID                        |
| userName              | admin               | String   | 用户名                        |
| gender                | 0                   | String   | 性别                          |
| phone                 | 15500000000         | String   | 手机号                        |
| address               | 广州                | String   | 地址                          |
| email                 | example@qq.com      | String   | 电子邮箱，比如 example@qq.com |
| password              | 123456              | String   | 密码                          |
| status                | -                   | Number   | 状态                          |
| post                  | -                   | Number   | 职位                          |
| createTime            | 2022-06-06T10:16:02 | String   | 创建时间                      |
| updateTime            | 2022-06-06T10:16:05 | String   | 更新时间                      |
## /User接口测试/list

#### 接口URL

> http://localhost:8080/user/list

#### 请求方式

> GET

#### Content-Type

> json

#### 请求Body参数

```javascript
{
		"userId": 1,
		"userName": "王五",
		"gender": "0",
		"phone": "15500000000",
		"address": "广州",
		"email": null,
		"password": "123456",
		"status": 0,
		"post": 0,
		"createTime": "2022-06-06T10:16:02",
		"updateTime": "2022-06-06T10:16:05"
	}
```

| 参数名          | 示例值      | 参数类型 | 是否必填 | 参数描述                      |
| --------------- | ----------- | -------- | -------- | ----------------------------- |
| userId          | 1           | Number   | 否       | 用户ID                        |
| userName        | 王五        | String   | 否       | 用户名                        |
| gender          | 0           | String   | 否       | 性别                          |
| phone           | 15500000000 | String   | 否       | 手机号                        |
| address         | 广州        | String   | 否       | 地址                          |
| email           | -           | Object   | 否       | 电子邮箱，比如 example@qq.com |
| password        | 123456      | String   | 否       | 密码                          |
| status          | -           | Number   | 否       | 状态                          |
| post            | -           | Number   | 否       | 职位                          |


#### 成功响应示例

```javascript
[
	{
		"userId": 1,
		"userName": "王五",
		"gender": "0",
		"phone": "15500000000",
		"address": "广州",
		"email": null,
		"password": "123456",
		"status": 0,
		"post": 0,
		"createTime": "2022-06-06T10:16:02",
		"updateTime": "2022-06-06T10:16:05"
	}
]
```

## /User接口测试/delete


#### 接口URL

> http://localhost:8080/user/delete?id=2

#### 请求方式

> DELETE

#### Content-Type

> form-data

#### 请求Query参数

| 参数名          | 示例值 | 参数类型 | 是否必填 | 参数描述 |
| --------------- | ------ | -------- | -------- | -------- |
| id              | 2      | Text     | 是       | 用户ID   |


## /User接口测试/add

#### 接口URL

> http://localhost:8080/user/add

#### 请求方式

> PUT

#### Content-Type

> json

#### 请求Body参数

```javascript
{
	"userName": "王五",
	"gender": "0",
	"phone": "15500000000",
	"address": "广州",
	"email": null,
	"password": "123456",
	"status": 0,
	"post": 0,
	"createTime": "2022-06-06T10:16:02",
	"updateTime": "2022-06-06T10:16:05"
}
```

#### 成功响应示例

```javascript
{
	"userId": null,
	"userName": "王五",
	"gender": "0",
	"phone": "15500000000",
	"address": "广州",
	"email": null,
	"password": "123456",
	"status": 0,
	"post": 0,
	"createTime": "2022-06-06T10:16:02",
	"updateTime": "2022-06-06T10:16:05"
}
```

| 参数名     | 示例值              | 参数类型 | 参数描述                      |
| ---------- | ------------------- | -------- | ----------------------------- |
| userId     | -                   | Number   | 用户ID                        |
| userName   | admi2222            | String   | 用户名                        |
| gender     | 0                   | String   | 性别                          |
| phone      | 15500000000         | String   | 手机号                        |
| address    | 广州                | String   | 地址                          |
| email      | -                   | String   | 电子邮箱，比如 example@qq.com |
| password   | 123456              | String   | 密码                          |
| status     | -                   | Number   | 状态                          |
| post       | -                   | Number   | 职位                          |
| createTime | 2022-06-06T10:16:02 | DateTime | 创建时间                      |
| updateTime | 2022-06-06T10:16:05 | DateTime | 更新时间                      |