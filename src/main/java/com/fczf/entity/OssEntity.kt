package com.fczf.entity

import com.baomidou.mybatisplus.annotation.IdType
import com.baomidou.mybatisplus.annotation.TableId
import com.baomidou.mybatisplus.annotation.TableName
import java.time.LocalDateTime

/**
 * 附件实体层
 * Create in 2022/6/9. 16:37
 * @author FuYuanxue fuc888@88.com
 */
@TableName("sys_oss")
data class OssEntity(
        @TableId("file_id", type = IdType.INPUT)
        var fileId: Long? = null,
        var fileName: String? = null,
        var fileSize: Int? = null,
        var fileContent: String? = null,
        var fileType: String? = null,
        var fileMd5: String? = null,
        var fileUrl: String? = null,
        var createTime: LocalDateTime? = null,
        var updateTime: LocalDateTime? = null
)