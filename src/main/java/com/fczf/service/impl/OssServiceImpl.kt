package com.fczf.service.impl

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl
import com.fczf.dao.OssDao
import com.fczf.entity.OssEntity
import com.fczf.service.OssService
import com.jcraft.jsch.ChannelSftp
import com.jcraft.jsch.JSch
import com.jcraft.jsch.Session
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.io.FileInputStream

/**
 * 附件接口实现层
 * Create in 2022/6/9. 16:45
 * @author FuYuanxue fuc888@88.com
 */
@Service
class OssServiceImpl : ServiceImpl<OssDao, OssEntity>(), OssService {
    override fun getList(): MutableList<OssEntity> {
        return baseMapper.selectList(QueryWrapper<OssEntity>())
    }

    override fun info(id: Long): OssEntity? {
        return baseMapper.selectById(id)
    }

    override fun delete(id: Long) {
        baseMapper.deleteById(id);
    }

    override fun add(multipartFile: MultipartFile): OssEntity {

        val file = this.multipartFileToFile(multipartFile);

        val fileToFileInputStream = this.fileToFileInputStream(file)

        val session = this.connectSSH("xxxxx", 22, "xxxxx", "xxxxx");

        this.uploadFileToServer(session, "/opt/nginx/html/static/" + multipartFile.originalFilename, fileToFileInputStream)

        val ossEntity = OssEntity().apply {
            this.fileName = multipartFile.originalFilename
            this.fileSize = multipartFile.size.toInt()
            this.fileType = multipartFile.contentType
            this.fileUrl = "http://xxxxx/static/" + multipartFile.originalFilename
        }

        baseMapper.insert(ossEntity);

        return ossEntity
    }

    override fun multipartFileToFile(multipartFile: MultipartFile): File {
        val originalFilename = multipartFile.originalFilename;
        val split: List<String> = originalFilename.toString().split(".")
        val file: File = File.createTempFile(split[0], "." + split[1]);
        multipartFile.transferTo(file);
        file.deleteOnExit();
        return file;
    }

    override fun connectSSH(host: String, port: Int, user: String, password: String): Session {
        var session: Session? = null
        val jsch = JSch()
        if (port != null) {
            session = jsch.getSession(user, host, port);
        } else {
            session = jsch.getSession(user, host);
        }
        session.setPassword(password);
        //设置第一次登陆的时候提示，可选值:(ask | yes | no)
        session.setConfig("StrictHostKeyChecking", "no");
        //30秒连接超时
        session.connect(30000);
        return session
    }

    override fun uploadFileToServer(session: Session, filePath: String, inputStream: FileInputStream) {
        try {
            var channel: ChannelSftp? = null
            channel = session.openChannel("sftp") as ChannelSftp
            channel.connect()
            channel.put(inputStream, filePath)
            if (channel != null) {
                channel.disconnect()
            }
        } catch (e: Exception) {
            log.warn("文件上传出错")
        }
    }

    override fun fileToFileInputStream(file: File): FileInputStream {
        return FileInputStream(file.path)
    }
}