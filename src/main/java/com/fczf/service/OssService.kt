package com.fczf.service

import com.baomidou.mybatisplus.extension.service.IService
import com.fczf.entity.OssEntity
import com.jcraft.jsch.Session
import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.io.FileInputStream

/**
 * OssService
 * Create in 2022/6/9. 16:45
 * @author FuYuanxue fuc888@88.com
 */
interface OssService : IService<OssEntity> {

    fun getList(): MutableList<OssEntity>

    fun info(id: Long): OssEntity?

    fun delete(id: Long)

    fun add(multipartFile: MultipartFile):OssEntity

    fun multipartFileToFile(multipartFile: MultipartFile): File

    fun connectSSH(host: String, port: Int, user: String, password: String): Session

    fun uploadFileToServer(session: Session, filePath: String, inputStream: FileInputStream)

    fun fileToFileInputStream(file: File): FileInputStream


}