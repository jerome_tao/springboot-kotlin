package com.fczf.controller

import com.fczf.service.OssService
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

/**
 * OssController
 * Create in 2022/6/9. 16:33
 * @author FuYuanxue fuc888@88.com
 */
@RestController
@RequestMapping("/oss")
class OssController(private val ossService: OssService) {


    @GetMapping("list")
    fun list() = ossService.getList()

    @GetMapping("info")
    fun info(id: Long) = ossService.info(id)

    @PostMapping("delete")
    fun delete(id: Long) = ossService.delete(id)

    @PostMapping("add")
    fun delete(@RequestParam file: MultipartFile) = ossService.add(file)


}