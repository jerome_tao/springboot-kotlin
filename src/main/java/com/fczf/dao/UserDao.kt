package com.fczf.dao

import com.baomidou.mybatisplus.core.mapper.BaseMapper
import com.fczf.entity.UserEntity
import org.apache.ibatis.annotations.Mapper

/**
 * 用户Dao层
 * Create in 2022/6/8.
 * @author FuYuanxue fuc888@88.com
 */
@Mapper
interface UserDao:BaseMapper<UserEntity> {
}