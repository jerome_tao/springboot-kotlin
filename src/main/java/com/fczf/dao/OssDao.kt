package com.fczf.dao

import com.baomidou.mybatisplus.core.mapper.BaseMapper
import com.fczf.entity.OssEntity
import org.apache.ibatis.annotations.Mapper

/**
 * 附件Dao层
 * Create in 2022/6/9. 16:37
 * @author FuYuanxue fuc888@88.com
 */
@Mapper
interface OssDao : BaseMapper<OssEntity> {
}